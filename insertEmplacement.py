
# coding: utf-8
import cgi
import psycopg2
#import requests

exec(compile(source=open('debug.py').read(), filename='debug.py', mode='exec'))

form = cgi.FieldStorage()

if form.keys() != []:

    # Variables locales qui permettent de recuperer les valeurs du formulaire
    numemplace = form["numemplace"].value
    numtype = form["numtype"].value
    
    logging.info("--------Insertion d'un nouveau client ----------")
    logging.info('numemplace = ' + numemplace)
    logging.info('numtype = ' + numtype)

    requete = """   insert into projet.emplacement 
                    (numemplace, numtype)
                    values ('%s','%s')
    """
    
    try:
        cur = conn.cursor()
        cur.execute( requete % (numemplace, numtype))
        conn.commit()
        logging.info("emplacement créé")
    except psycopg2.IntegrityError as error : 
        logging.error("Insertion d'un nouvel emplacement : en erreur")
        logging.error(error)
        conn.rollback()
    
    finally:
        logging.info("Fin de l'insertion d'un nouvel emplacement")
        cur.close()
