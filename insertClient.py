
# coding: utf-8
import cgi
import psycopg2
#import requests

exec(compile(source=open('debug.py').read(), filename='debug.py', mode='exec'))
#exec(compile(source=open('pageDebutConnexion.py').read(), filename='pageDebutConnexion.py', mode='exec'))

requete = """   insert into projet.client 
                (numclient, nomclient, prenomclient, cp, rue, ville)
                values ('%s','%s','%s','%s','%s','%s')
"""

def createClient(numclient,nomclient,prenomclient,cp,rue,ville) :
    
    try:
        cur = conn.cursor()
        cur.execute( requete % (numclient,nomclient,prenomclient,cp,rue,ville))
        conn.commit()
        logging.info("Le client a ete insére")
    except psycopg2.IntegrityError as error : 
        logging.error("Insertion d'un nouveau client : en erreur")
        logging.error(error)
        conn.rollback()
    
    finally:
           #Close connection with DatabaseError
        if(conn):
            logging.info("Fin de l'insertion d'un nouveau client")
            cur.close()

            #conn.close()
            #url = "http://localhost:8888/pageClient.py"
            #url = "pageClient.py"
            #r = requests.head(url, allow_redirects=True)
            #print(r.url)            
            #res = urllib.request.urlopen(url)
            #finalurl = res.geturl()
            #print(finalurl)
            #responses = requests.get(url)
            #requests.head(url, allow_redirects=True)
            #requests.url


# Recuperation des variables du fichier views.py
form = cgi.FieldStorage()

if form.keys() != []:

    # Variables locales qui permettent de recuperer les valeurs du formulaire
    numero = form["numero"].value
    nom = form["nom"].value
    prenom = form["prenom"].value
    adresse = form["adresse"].value
    ville = form["ville"].value
    cp = form["cp"].value

    logging.info("--------Insertion d'un nouveau client ----------")
    logging.info('numero = ' + numero)
    logging.info('nom = ' + nom)
    logging.info('prenom = ' + prenom)
    logging.info('adresse = ' + adresse)
    logging.info('ville = ' + ville)
    logging.info('cp = ' + cp)

    createClient(numero,nom,prenom,cp, adresse,ville)
