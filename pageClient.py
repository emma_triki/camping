
# coding: utf-8
import cgi
import psycopg2

exec(compile(source=open('pageDebut.py').read(), filename='pageDebut.py', mode='exec'))
exec(compile(source=open('pageDebutConnexion.py').read(), filename='pageDebutConnexion.py', mode='exec'))

sql = """select numclient, prenomclient, nomclient, rue, cp, ville
        from projet.client 
         """
cursor = conn.cursor()
cursor.execute( sql )

print ( """           
            <div class="titre">Liste des clients</div>
                <table class="tab"  style="width: 100% !important">
                <tr>
                    <th style="width: 10%">Numero</th>
                    <th style="width: 20%">Prenom</th>
                    <th style="width: 20%">Nom</th>
                    <th style="width: 50%">Adresse</th>
                </tr>
        """
)

for numclient, prenomclient, nomclient, rue, cp, ville in cursor.fetchall() :
    print(  """<tr><td>n&deg;""" , numclient ,
            """</td><td>""" , prenomclient , 
            """</td><td>""", nomclient, 
            """</td><td>""", rue, cp, ville,
            """</td></tr> """ )

print ( """
    </table>
    <br/>
    <div style="width: 100%; text-align: center">
        <a href="pageCreerClient.py">Cr&eacute;er un nouveau client</a>
    </div>
    """
)

conn.close()

exec(compile(source=open('pageFin.py').read(), filename='pageFin.py', mode='exec'))
