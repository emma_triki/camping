--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.19
-- Dumped by pg_dump version 9.6.19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: projet; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA projet;


ALTER SCHEMA projet OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: client; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.client (
    numclient integer NOT NULL,
    nomclient character varying(10),
    prenomclient character varying(10),
    datedenaissance date,
    cp integer,
    rue character varying(10),
    ville character varying(10),
    telephone integer,
    mail character varying(10),
    numgroupe integer
);


ALTER TABLE projet.client OWNER TO postgres;

--
-- Name: comprend; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.comprend (
    numtarif integer NOT NULL,
    numsup integer NOT NULL
);


ALTER TABLE projet.comprend OWNER TO postgres;

--
-- Name: depend; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.depend (
    numtarif integer NOT NULL,
    nomsaison character varying(10) NOT NULL
);


ALTER TABLE projet.depend OWNER TO postgres;

--
-- Name: emplacement; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.emplacement (
    numemplace integer NOT NULL,
    numtype integer
);


ALTER TABLE projet.emplacement OWNER TO postgres;

--
-- Name: groupe; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.groupe (
    numgroupe integer NOT NULL
);


ALTER TABLE projet.groupe OWNER TO postgres;

--
-- Name: reservation; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.reservation (
    numreser integer NOT NULL,
    datedebutsejour date,
    datefinsejour date,
    numemplace integer,
    numclient integer
);


ALTER TABLE projet.reservation OWNER TO postgres;

--
-- Name: saison; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.saison (
    nomsaison character varying(10) NOT NULL,
    datedebutsaison date,
    datefinsaison date
);


ALTER TABLE projet.saison OWNER TO postgres;

--
-- Name: suplement; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.suplement (
    numsup integer NOT NULL,
    voiture integer,
    demip boolean
);


ALTER TABLE projet.suplement OWNER TO postgres;

--
-- Name: tarif; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.tarif (
    numtarif integer NOT NULL,
    duree integer,
    nbpersonne integer,
    numemplace integer
);


ALTER TABLE projet.tarif OWNER TO postgres;

--
-- Name: type; Type: TABLE; Schema: projet; Owner: postgres
--

CREATE TABLE projet.type (
    numtype integer NOT NULL,
    libelle character varying(10)
);


ALTER TABLE projet.type OWNER TO postgres;

--
-- Data for Name: client; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: comprend; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: depend; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: emplacement; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: groupe; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: reservation; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: saison; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: suplement; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: tarif; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Data for Name: type; Type: TABLE DATA; Schema: projet; Owner: postgres
--



--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (numclient);


--
-- Name: comprend comprend_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.comprend
    ADD CONSTRAINT comprend_pkey PRIMARY KEY (numtarif, numsup);


--
-- Name: depend depend_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.depend
    ADD CONSTRAINT depend_pkey PRIMARY KEY (numtarif, nomsaison);


--
-- Name: emplacement emplacement_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.emplacement
    ADD CONSTRAINT emplacement_pkey PRIMARY KEY (numemplace);


--
-- Name: groupe groupe_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.groupe
    ADD CONSTRAINT groupe_pkey PRIMARY KEY (numgroupe);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (numreser);


--
-- Name: saison saison_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.saison
    ADD CONSTRAINT saison_pkey PRIMARY KEY (nomsaison);


--
-- Name: suplement suplement_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.suplement
    ADD CONSTRAINT suplement_pkey PRIMARY KEY (numsup);


--
-- Name: tarif tarif_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.tarif
    ADD CONSTRAINT tarif_pkey PRIMARY KEY (numtarif);


--
-- Name: type type_pkey; Type: CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.type
    ADD CONSTRAINT type_pkey PRIMARY KEY (numtype);


--
-- Name: depend fk_nomsaison_depend; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.depend
    ADD CONSTRAINT fk_nomsaison_depend FOREIGN KEY (nomsaison) REFERENCES projet.saison(nomsaison);


--
-- Name: reservation fk_numclient_reservation; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.reservation
    ADD CONSTRAINT fk_numclient_reservation FOREIGN KEY (numclient) REFERENCES projet.client(numclient);


--
-- Name: reservation fk_numemplace_reservation; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.reservation
    ADD CONSTRAINT fk_numemplace_reservation FOREIGN KEY (numemplace) REFERENCES projet.emplacement(numemplace);


--
-- Name: tarif fk_numemplace_tarif; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.tarif
    ADD CONSTRAINT fk_numemplace_tarif FOREIGN KEY (numemplace) REFERENCES projet.emplacement(numemplace);


--
-- Name: client fk_numgroupe_client; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.client
    ADD CONSTRAINT fk_numgroupe_client FOREIGN KEY (numgroupe) REFERENCES projet.groupe(numgroupe);


--
-- Name: comprend fk_numsup_comprend; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.comprend
    ADD CONSTRAINT fk_numsup_comprend FOREIGN KEY (numsup) REFERENCES projet.suplement(numsup);


--
-- Name: comprend fk_numtarif_comprend; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.comprend
    ADD CONSTRAINT fk_numtarif_comprend FOREIGN KEY (numtarif) REFERENCES projet.tarif(numtarif);


--
-- Name: depend fk_numtarif_depend; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.depend
    ADD CONSTRAINT fk_numtarif_depend FOREIGN KEY (numtarif) REFERENCES projet.tarif(numtarif);


--
-- Name: emplacement fk_numtype_emplacement; Type: FK CONSTRAINT; Schema: projet; Owner: postgres
--

ALTER TABLE ONLY projet.emplacement
    ADD CONSTRAINT fk_numtype_emplacement FOREIGN KEY (numtype) REFERENCES projet.type(numtype);


--
-- PostgreSQL database dump complete
--

