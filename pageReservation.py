
# coding: utf-8
import cgi
import psycopg2
from urllib.parse import urlparse
import sys, os, io

exec(compile(source=open('debug.py').read(), filename='debug.py', mode='exec'))
exec(compile(source=open('pageDebut.py').read(), filename='pageDebut.py', mode='exec'))
exec(compile(source=open('pageDebutConnexion.py').read(), filename='pageDebutConnexion.py', mode='exec'))

# On récupére l'url pour connaitre le type demandé
urlChaine = os.environ.get('QUERY_STRING')
pos = urlChaine.find("=")
idTypeEmplacement = urlChaine[(pos+1):len(urlChaine)]

logging.info("------------ Recherche du type dans la base de données --------------------------")
requete = "select numtype, libelle from projet.type where numtype = " + idTypeEmplacement
cur0 = conn.cursor()
cur0.execute(requete)
row = cur0.fetchone()
typeEmplacement = row[1];
cur0.close()

logging.info("------------ Reservation requete --------------------------")

# requete pour avoir les numero de semaine
requete1 = "SELECT f_samedi[ %s ] as du,f_samedi[ %s ]+7 as au FROM F_Samedi()"

# requete pour connaitre le nombre d'emplacement pour un type donné
requete2 = "select * from projet.emplacement where numtype = " + idTypeEmplacement

# requete pour connaitre les résevations pour un type d'emplacement donné et une date de début
requete3 = """
            select r.numemplace from projet.reservation r inner join projet.emplacement e on e.numemplace = r.numemplace  
            where datedebutsejour=%s
            and numtype = """ + idTypeEmplacement

cur1 = conn.cursor()
cur2 = conn.cursor()

print   ( """
        <br/>
        <div class="titre">Tableau de reservation : """, typeEmplacement , """</div>
        <table>
            <tr>
                <th>Numero de semaine</th>
                <th>Du</th>
                <th>Au</th>
        """)

cur2.execute( requete2 )

listEmplacement = []
i = 0
for numemplace, numtype in cur2.fetchall() :
    print(  """<th>Place n&deg;""" , numemplace ,"""</th>""" )
    listEmplacement.append(numemplace)

print("</tr>")

for compteur in range(15):

    data_tuple = (compteur,compteur)
    cur1.execute( requete1, data_tuple )
    print("""<tr>
             <td>Semaine """ , compteur , """</td>""")

    for du,au in cur1.fetchall() :
        print(     """<td>"""  , du , """</td><td>""" , au , """</td>"""    )
        
        listEnregistrement = []
        cur3 = conn.cursor()
        cur3.execute( requete3, [du] )
        
        for enr in cur3.fetchall() :
            listEnregistrement.append(enr[0])
 
        for n in listEmplacement:
            if n in listEnregistrement :
                print("""<td class="red"></td> """)    
            else:
                print("""<td><input type="button" value="Reserver" /></td>""")
        
                
print (
    """
    </table>
    """)

cur2.close()
cur1.close()
conn.close()

exec(compile(source=open('pageFin.py').read(), filename='pageFin.py', mode='exec'))
