
# coding: utf-8
import cgi
import psycopg2

exec(compile(source=open('pageDebut.py').read(), filename='pageDebut.py', mode='exec'))
exec(compile(source=open('pageDebutConnexion.py').read(), filename='pageDebutConnexion.py', mode='exec'))

sql = """select e.numemplace, e.numtype, t.libelle
         from projet.emplacement e 
         inner join projet.type t on e.numtype = t.numtype"""
cursor = conn.cursor()
cursor.execute( sql )

print ( """           
            <div class="titre">Liste des emplacements</div>
            <table class="tab">
            <tr>
                <th>Numero d'emplacement</th>
                <th>Numero de type</th>
                <th>Libelle type</th>
            </tr>
        """
)

for numemplace,numtype,libelle in cursor.fetchall() :
    print(  """<tr><td >n&deg;""" , numemplace ,"""</td><td>""" , numtype , """</td><td>""", libelle, """</td></tr> """ )

print ( """
    </table>
    """
)

conn.close()

exec(compile(source=open('pageFin.py').read(), filename='pageFin.py', mode='exec'))
