
# coding: utf-8

import cgi
import psycopg2

hostname = 'localhost'
# port = '5432' le port est par défaut 5432
username = 'postgres'
password = 'postgres'
database = 'postgres'

conn = psycopg2.connect( host=hostname, user=username, password=password, dbname=database )
